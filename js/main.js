/**
 * Created by MyLittleWolrd on 08.11.2017.
 */

(function() {
//add event to form
    document.forms["favorite"].onsubmit = function eventSubmit(){
        createItem();
    };

    var list = document.getElementById('messageList');
    var maskKey = "keyList_comment";
    var maxCountItem = 5;

    var main_page;
    var li_item;
    var countPages;

    addItemDefault();

// Added default records to localStorage
    function addItemDefault(){
        if(!localStorage.length){
            var massItem={"Item":[]};
            massItem.Item.push({name:'Петька',surname:'Разгельдяев',comment:'Всем привет!'});
            massItem.Item.push({name:'Васька', surname:'Букин',       comment:'хай'});
            massItem.Item.push({name:'Расиль', surname:'Зайнутдинов', comment:'cssHat  и прям в фотошопе смотришь css, или extract для brackets, так вообще фотошопе не нужен. Но это конечно в теории. Зачем это нужно? например, чтобы не ковырятся с градиентом,прозрачностью, бордер-радиусом. Такое за 10 секунд не напечатаешь, придётся активно пощелкать мышкой. А с плагином: выделил слой, нажал кнопочку, скопипастил css. Жаль я узнал про это все, когда решил отойти от вёрстки в бэк-енд. Ну да ладно, в жизни,как говорится,все пригодится.'});
            massItem.Item.push({name:'Евгений',surname:'Кремлёв',     comment:'В версии СС (Photoshop, Illustrator) по умолчанию идет эта функция, без всяких плагинов и генерации кода на левом сайте'});
            massItem.Item.push({name:'Сашка',  surname:'',            comment:'что ж, думайте дальше, это ваше право.'});
            massItem.Item.push({name:'Denis',  surname:'Borisov',     comment:'ВНИМАНИЕ: только что обнаружил, что на моем iPad, если включить анонимный режим (private mode), localStorage перестает поддерживаться. То же самое касается других продуктов apple. В обычном режиме браузинга localStorage работает, а в приватном режиме - нет, выкидывается ошибка и js скрипты перестают работать. Некоторые js-библиотеки используют local-storage без проверки того, работает ли он в данном окружении - и поэтому ломаются на анонимном режиме iPad Желаю apple разориться.'});
            massItem.Item.push({name:'Дима',   surname:'Гашко',       comment:'(хотя он, наконец-то, будет добавлен в IE 9) Будет? Какого года эта статья?'});
            massItem.Item.push({name:'андрей', surname:'маринин',     comment:'Подскажите как подключить LocalStorage к отчету обратного времени Куда мне прописать хранилище для сохранения отчета после обновления браузера Код скрипта отчета времени'});
            massItem.Item.push({name:'Кир',    surname:'Головин',     comment:'Я не в курсе кто переводил, или кто писал, но искренне надеюсь что познания этих людей в истории IT увеличатся после этого коммента. Adobe купила флеш только в 2005-2006 году. Масяня делалась на Macromedia Flash. Adobe НИ КОИМ ОБРАЗОМ не могла в Flash 6! Они еще дебагили акробат риадер 6ой версии, если не ошибаюсь! Adobe конечно сделало своих кучек в развитии веб технологий, но валить на них, то что они сделать не могли - как минимум некорректно!'});
            massItem.Item.push({name:'Сергей', surname:'Николаев',    comment:'Смысл? При удалении браузера (например переустановлена ОС), все данные с localstorage теряются. Если только для игр использовать или для больших вычислений... И то, последнее разумнее в среде ОС делать. Или я вас не до конца понял.'});
            massItem.Item.push({name:'Дмитрий',surname:'Валуев',      comment:'А почему примеры не показываете например в jsbin? Удобно и просто.﻿!'});

            massItem.Item.forEach(function(object){
                addItemLocalSt(object);
            });
        }

        // обновление содержимого Html страницы
        showItemAll();
    }

// Added records to localStorage
    function addItemLocalSt(objItem){
        var massItem;
        if(localStorage.getItem(maskKey) == null){
            massItem = {'Item':[]};
        }else{
            try {
                massItem = JSON.parse(localStorage.getItem(maskKey));
            }
            catch (err) {
                massItem = {'Item':[]};
                console.log('Error ' + err.name + ":" + err.message + "\n" + err.stack);
            }
        }
        massItem.Item.push(objItem);

        localStorage.setItem(maskKey, JSON.stringify(massItem));
    }

// Formation records to Html
    function addItemHtml(keyId, strUser, strSurname, strCommit){
        var newLine = document.createElement('li');
        newLine.className = "num " + keyId;
        newLine.innerHTML = "<div>" +
            "<p class= 'message-user'>" + strUser + (strSurname.length > 0 ? " " + strSurname: "") + "</p>" +
            "<p class= 'message-comment'>" + strCommit + "</p>" +
            "</div>";
        return newLine;
    }

// Display records to Html from localStorage
    function showItemAll(){
        // clear nodes to Html
        while (list.lastChild)
            list.removeChild(list.lastChild);

        // Formation records
        if (!localStorage.getItem(maskKey)) {
        } else {
            var massItem = JSON.parse(localStorage.getItem(maskKey)).Item;
            for (var i = 0; i < massItem.length; i++) {
                list.insertBefore(addItemHtml(maskKey + "_" + i, massItem[i].name, massItem[i].surname, massItem[i].comment), list.firstChild);
            }

            // Removal records by event click
            addEventClickItem();

            // Pagination Pages
            pagination();
        }

    }

// Data transferring to localStorage and Html
    function createItem(){
        var element = document.getElementById('parent').children;
        if(element[0].value.length > 0 && element[2].value.length > 0){

            var strUser = element[0].value;
            element[0].value = "";

            var strSurname = element[1].value.length > 0 ? element[1].value: "";
            element[1].value = "";

            var strCommmit = element[2].value;
            element[2].value = "";

            // Data recording to Html
            var keyId = maskKey + "_" + (JSON.parse(localStorage.getItem(maskKey)).Item).length;
            list.insertBefore(addItemHtml(keyId, strUser, strSurname, strCommmit), list.firstChild);

            // Data recording to localStorage
            addItemLocalSt({name:strUser,surname:strSurname,comment:strCommmit});

            // Update contents Html page
            showItemAll();
        }else {
            if (element[0].value.length == 0) {
                alert('Please enter a name');
            } else {
                alert('Please enter a comment');
            }
        }
    }

// Deleting records Event click
    function deleteItem(){
        // Deleted selection item from Array
        var obj = this;
        var keyId = obj.getAttribute('class').slice(maskKey.length+5);

        // Deleted object from Array in localStorage
        var massItem=JSON.parse(localStorage.getItem(maskKey));
        massItem.Item.splice(keyId,1);

        // Rewrite array in localStorage
        localStorage.removeItem(maskKey);
        massItem.Item.forEach(function(object){
            addItemLocalSt(object);
        });

        // Update content for Html page
        showItemAll();
    }

// Added Events click from records
    function addEventClickItem(){
        var elEvent = list.querySelectorAll('li');
        for(var i = 0; i < elEvent.length; i++){
            elEvent[i].addEventListener('click',deleteItem,true);
        }
    }


// -------------------- PAGINATION --------------------

// Create pagination
    function pagination(){
        var navPages = document.querySelector('.pagination-pages');

        // Events go to pages
        navPages.onclick = function eventPage(event){
            casePage(event);
        };

        //Count pages
        countPages=Math.ceil(JSON.parse(localStorage[maskKey]).Item.length/maxCountItem);

        // Formation list of page
        var page = "";
        for(var i = 0; i < countPages; i++){
            page += "<span data-page = "+ (i * maxCountItem) +" id = \"page" + (i + 1) + "\">" + (i + 1) + "</span>";
        }
        navPages.innerHTML = page;

        // Output of the first (maxCountItem) records
        li_item = list.querySelectorAll(".num");
        for(i = 0; i < li_item.length; i++){
            if(i < maxCountItem){
                li_item[i].style.display = "block";
            }
        }
        main_page = document.getElementById("page1");
        main_page.classList.add("pagination-pages_active");
    }

// Go to Pages
    function casePage(event){
        var e = event || window.event;
        var target = e.target;
        var id = target.id;

        if(target.tagName.toLowerCase() != "span") return; // Do you need a test ???

        // Reassign active page
        main_page.classList.remove("pagination-pages_active");
        main_page = document.getElementById(id);
        main_page.classList.add("pagination-pages_active");

        // All records invisible
        var data_page = +target.dataset.page;
        li_item.forEach(function(object){
            object.style.display = "none";
        });

        // Visible select records from page
        for(var i = data_page, j = 0; i < li_item.length; i++){
            if(j >= maxCountItem)
                break;
            li_item[i].style.display = "block";
            j++;
        }
    }
})();